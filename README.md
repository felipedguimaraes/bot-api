# BOT API

# Table of contents

- [Application Checklist](#application-checklist)
- [Architecture](#architecture)
    - [Build requirements](#build-requirements)
    - [Frameworks used](#frameworks-used)
- [Setup](#setup)
    - [How to Run](#how-to-run)
    - [How to Test](#how-to-test)



## Application Checklist

- [  ] Continuous integration
- [  ] Continuous delivery


## Architecture

### Build requirements

In order to build and run Bot application you need to have some installed:

- JDK 11
- Setup `JAVA_HOME` environment variables with path to JDK 11
- MySQL with root/root password :P

### Frameworks used

- [**Spring Boot**](https://github.com/spring-projects/spring-boot)

### How to Run

First things first:

```sh
./mvnw spring-boot:run
```

#### Configuring the project into an IDE

Simply import the project as a maven project. Once you have configured your project 

Run the BotApplication [java main class](src/main/java/com/vivareal/com.vivo.challenge/BotApplication.java)


### How to Test

#### Unit tests

Sorry, its a debt :(

