package com.vivo.challenge.domain.repository;

import com.vivo.challenge.domain.model.Bot;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BotRepository extends JpaRepository<Bot, UUID> {

}
