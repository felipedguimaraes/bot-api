package com.vivo.challenge.domain.model;

import java.util.Date;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
public class Message {

  @Id
  @EqualsAndHashCode.Include
  @GeneratedValue(generator = "UUID")
  @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
  @Column(name = "message_id", updatable = false, nullable = false)
  @Type(type="uuid-char")
  private UUID id;

  @ManyToOne
  @JoinColumn(name = "conversation_id", nullable = false)
  private Conversation conversation;

  private Date timestamp;

  @Column(nullable = false)
  private String messageFrom;

  @Column(nullable = false)
  private String messageTo;

  @Column(nullable = false)
  private String text;

}
