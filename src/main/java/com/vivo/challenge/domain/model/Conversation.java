package com.vivo.challenge.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
public class Conversation {

  @Id
  @EqualsAndHashCode.Include
  @GeneratedValue(generator = "UUID")
  @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
  @Column(name = "conversation_id", updatable = false, nullable = false)
  @Type(type="uuid-char")
  private UUID id;

  @JsonIgnore
  @OneToMany(mappedBy = "conversation")
  private List<Message> messages = new ArrayList<>();

}
