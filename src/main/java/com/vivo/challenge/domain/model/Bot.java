package com.vivo.challenge.domain.model;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
public class Bot {

  @Id
  @EqualsAndHashCode.Include
  @GeneratedValue(generator = "UUID")
  @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
  @Column(name = "bot_id", updatable = false, nullable = false)
  @Type(type="uuid-char")
  private UUID id;

  private String name;
}
