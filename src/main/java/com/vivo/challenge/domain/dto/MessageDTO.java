package com.vivo.challenge.domain.dto;

import java.util.Date;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MessageDTO {
  private String id;
  private String conversationId;
  private Date timestamp;
  private String from;
  private String to;
  private String text;
}
