package com.vivo.challenge.domain.service;

import com.vivo.challenge.domain.exception.EntidadeEmUsoException;
import com.vivo.challenge.domain.exception.EntidadeNaoEncontradaException;
import com.vivo.challenge.domain.model.Conversation;
import com.vivo.challenge.domain.repository.ConversationRepository;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

@Service
public class ConversationService {
  @Autowired
  private ConversationRepository conversationRepository;

  public List<Conversation> findAll() {
    return conversationRepository.findAll();
  }

  public Conversation find(UUID id) {
    return conversationRepository.getOne(id);
  }

  public Conversation save(Conversation conversation) {
    return conversationRepository.save(conversation);
  }

  public void remove(UUID id) {
    try {
      var conversation = find(id);
      conversationRepository.delete(conversation);
    } catch (EmptyResultDataAccessException e) {
      throw new EntidadeNaoEncontradaException(
          String.format("Não existe uma conversa com código %d", id));
    } catch (DataIntegrityViolationException e) {
      throw new EntidadeEmUsoException(
          String.format("Conversa de código %d não pode ser removido, pois está em uso", id));
    }
  }
}
