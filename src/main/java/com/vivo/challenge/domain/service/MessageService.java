package com.vivo.challenge.domain.service;

import com.vivo.challenge.domain.exception.EntidadeEmUsoException;
import com.vivo.challenge.domain.exception.EntidadeNaoEncontradaException;
import com.vivo.challenge.domain.model.Conversation;
import com.vivo.challenge.domain.model.Message;
import com.vivo.challenge.domain.repository.ConversationRepository;
import com.vivo.challenge.domain.repository.MessageRepository;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

@Service
public class MessageService {
  @Autowired private MessageRepository messageRepository;

  @Autowired private ConversationRepository conversationRepository;

  public List<Message> findAll() {
    return messageRepository.findAll();
  }

  public Message find(UUID id) {
    return messageRepository.getOne(id);
  }

  public List<Message> findByConversation(UUID conversationId) {
    return messageRepository.findByConversationId(conversationId);
  }

  public Message save(Message message) {
    var conversation = message.getConversation();
    if (conversation != null && conversation.getId() != null){
      conversation = conversationRepository
          .findById(message.getConversation().getId()).orElse(null);
    } else {
      conversation = conversationRepository.save(new Conversation());
    }

    message.setConversation(conversation);
    return messageRepository.save(message);
  }

  public void remove(UUID id) {
    try {
      var message = find(id);
      messageRepository.delete(message);
    } catch (EmptyResultDataAccessException e) {
      throw new EntidadeNaoEncontradaException(
          String.format("Não existe um cadastro de mensagem com código %s", id.toString()));
    } catch (DataIntegrityViolationException e) {
      throw new EntidadeEmUsoException(
          String.format(
              "Mensagem de código %s não pode ser removido, pois está em uso", id.toString()));
    }
  }
}
