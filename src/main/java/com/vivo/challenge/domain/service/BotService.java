package com.vivo.challenge.domain.service;

import com.vivo.challenge.domain.exception.EntidadeEmUsoException;
import com.vivo.challenge.domain.exception.EntidadeNaoEncontradaException;
import com.vivo.challenge.domain.model.Bot;
import com.vivo.challenge.domain.repository.BotRepository;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

@Service
public class BotService {

  @Autowired private BotRepository botRepository;

  public List<Bot> findAll() {
    return botRepository.findAll();
  }

  public Bot find(UUID id) {
    return botRepository.getOne(id);
  }

  public Bot save(Bot bot) {
    return botRepository.save(bot);
  }

  public void remove(UUID id) {
    try {
      var bot = find(id);
      botRepository.delete(bot);
    } catch (EmptyResultDataAccessException e) {
      throw new EntidadeNaoEncontradaException(
          String.format("Não existe um cadastro de bot com código %s", id.toString()));
    } catch (DataIntegrityViolationException e) {
      throw new EntidadeEmUsoException(
          String.format("Bot de código %s não pode ser removido, pois está em uso", id.toString()));
    }
  }
}
