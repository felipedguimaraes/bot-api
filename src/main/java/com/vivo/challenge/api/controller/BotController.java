package com.vivo.challenge.api.controller;

import com.vivo.challenge.domain.exception.EntidadeEmUsoException;
import com.vivo.challenge.domain.exception.EntidadeNaoEncontradaException;
import com.vivo.challenge.domain.model.Bot;
import com.vivo.challenge.domain.service.BotService;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/bots")
public class BotController {

  @Autowired
  private BotService botService;

  @GetMapping
  public List<Bot> getAll() {
    return botService.findAll();
  }

  @GetMapping("/{id}")
  public ResponseEntity<Bot> buscar(@PathVariable UUID id) {
    Bot bot = botService.find(id);
    if (bot != null) {
      return ResponseEntity.ok(bot);
    }

    return ResponseEntity.notFound().build();
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public ResponseEntity<Bot> salvar(@RequestBody Bot bot) {
    Bot actualBot = botService.save(bot);
    return ResponseEntity.status(HttpStatus.CREATED).body(actualBot);
  }

  @PutMapping("/{id}")
  public ResponseEntity<?> atualizar(@PathVariable UUID id, @RequestBody Bot bot) {
    Bot actualBot = botService.find(id);
    try {
      if (actualBot != null) {
        BeanUtils.copyProperties(bot, actualBot, "id");
        botService.save(actualBot);
        return ResponseEntity.ok(actualBot);
      }
      return ResponseEntity.notFound().build();
    } catch (EntidadeNaoEncontradaException e) {
      return ResponseEntity.badRequest().body(e.getMessage());
    }
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<?> remover(@PathVariable UUID id){
    try {
      botService.remove(id);
      return ResponseEntity.noContent().build();
    } catch (EntidadeNaoEncontradaException e){
      return ResponseEntity.notFound().build();
    } catch (EntidadeEmUsoException e){
      return ResponseEntity.status(HttpStatus.CONFLICT).body(e.getMessage());
    }
  }
}
