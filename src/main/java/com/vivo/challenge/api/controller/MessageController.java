package com.vivo.challenge.api.controller;

import com.vivo.challenge.domain.dto.MessageDTO;
import com.vivo.challenge.domain.model.Conversation;
import com.vivo.challenge.domain.model.Message;
import com.vivo.challenge.domain.service.ConversationService;
import com.vivo.challenge.domain.service.MessageService;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/messages")
public class MessageController {

  @Autowired
  private MessageService messageService;

  @Autowired
  private ConversationService conversationService;

  private String uuid = UUID.randomUUID().toString();

  @GetMapping
  public List<MessageDTO> findAll(@RequestParam(value = "conversationId", required = false) UUID conversationId) {
    if (conversationId != null) {
      return toCollectionModel(messageService.findByConversation(conversationId));
    }
    return toCollectionModel(messageService.findAll());
  }

  @GetMapping("/{id}")
  public ResponseEntity<MessageDTO> buscar(@PathVariable UUID id) {
    Message message = messageService.find(id);
    if (message != null) {
      return ResponseEntity.ok(toRepresentationalObject(message));
    }

    return ResponseEntity.notFound().build();
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public ResponseEntity<MessageDTO> salvar(@RequestBody @Valid MessageDTO message) {
    Message actualMessage = messageService.save(toDomainObject(message));
    return ResponseEntity.status(HttpStatus.CREATED).body(toRepresentationalObject(actualMessage));
  }

  private Message toDomainObject(MessageDTO messageDTO){
    var message = new Message();
    var conversation = new Conversation();

    if (messageDTO.getConversationId() != null){
      conversation.setId(UUID.fromString(messageDTO.getConversationId()));
    }
    message.setConversation(conversation);
    if(messageDTO.getId() != null){
      message.setId(UUID.fromString(messageDTO.getId()));
    }
    message.setMessageFrom(messageDTO.getFrom());
    message.setMessageTo(messageDTO.getTo());
    message.setText(messageDTO.getText());
    message.setTimestamp(messageDTO.getTimestamp());
    return message;
  }

  private MessageDTO toRepresentationalObject(Message message){
    var messageDTO = new MessageDTO();
    messageDTO.setConversationId(message.getConversation().getId().toString());
    messageDTO.setFrom(message.getMessageFrom());
    messageDTO.setTo(message.getMessageTo());
    messageDTO.setId(message.getId().toString());
    messageDTO.setTimestamp(message.getTimestamp());
    messageDTO.setText(message.getText());
    return messageDTO;
  }

  private List<MessageDTO> toCollectionModel(List<Message> messages){
    return messages.stream().map(message -> toRepresentationalObject(message))
        .collect(Collectors.toList());
  }

}
